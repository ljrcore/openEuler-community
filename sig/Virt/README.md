# Virt

* 负责openEuler虚拟化相关组件社区技术发展和决策
* 负责openEuler虚拟化相关软件包的规划、升级和维护
* 及时响应openEuler虚拟化产品用户反馈和解决虚拟化相关问题


# 组织会议

- 每双周周五下午2:30-4:30
- 通过邮件申报议题



# 成员


### Maintainer列表

- zhanghailiang[@zhanghailiang](https://gitee.com/zhanghailiang_lucky)
- fangying[@fangying](https://gitee.com/yorifang)
- xuyandong[@xuyandong](https://gitee.com/xydong)
- wubin[@wubin](https://gitee.com/RootWB)
- wangzhigang[@wangzhigang](https://gitee.com/cellfaint)

### Committer列表

- pannengyuan[@pannengyuan](https://gitee.com/panny060)
- chenqun[@chenqun](https://gitee.com/kuhnchen18)
- xufei[@xufei](https://gitee.com/flyking001)
- zhangliang[@zhangliang](https://gitee.com/zhangliang5)



# 联系方式

- [邮件列表](https://mailweb.openeuler.org/postorius/lists/virt.openeuler.org/): virt@openeuler.org
- [IM](#openeuler-dev)


# 项目清单

- KVM + QEMU
    - kvm
        - repository地址：https://gitee.com/openeuler/kernel
    - qemu
        - repository地址：https://gitee.com/openeuler/qemu
    - libvirt
        - repository地址：https://gitee.com/openeuler/libvirt
    - edk2
        - repository地址：https://gitee.com/src-openeuler/edk2
    - vmtop
        - repository地址：https://gitee.com/openeuler/vmtop
- StratoVirt
    - stratovirt
        - repository地址：https://gitee.com/openeuler/stratovirt
- LibcarePlus
    - libcareplus
        - repository地址：https://gitee.com/openeuler/libcareplus
