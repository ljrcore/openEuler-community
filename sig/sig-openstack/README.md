# openEuler openstack SIG

## 使命和愿景

OpenStack是美国国家航空航天局和Rackspace合作研发的云计算软件，以Apache授权条款授权，并且是自由和开放源代码软件。
OpenStack作为第二大开源社区，在全球范围拥有众多个人及企业组织提供代码贡献。openEuler openstack SIG致力于贡为openstack社区贡献更适合行业发展的平台功能增强，
并且定期组织会议为社区发展提供建议和回馈。


## SIG 工作目标和范围

- 在 openEuler 之上提供原生的 openstack 及功能patch
- 定期召开会议讨论openstack社区发展

## 组织会议

公开的会议时间：待定


## 成员

### Maintainer列表

- 陈硕[@joec88](https://gitee.com/joec88)

### Committer列表

- 陈硕[@joec88](https://gitee.com/joec88)
- 李昆山[@liksh](https://gitee.com/liksh)
- 高松[@the-moon-is-blue](https://gitee.com/the-moon-is-blue)
- 张迎[@zhangy1317](https://gitee.com/zhangy1317)

### 联系方式

- [邮件列表](dev@openeuler.org)


## 项目清单


###项目名称：

repository地址：

- https://gitee.com/src-openeuler/openstack-nova
- https://gitee.com/src-openeuler/openstack-swift
- https://gitee.com/src-openeuler/openstack-keystone
- https://gitee.com/src-openeuler/openstack-neutron
- https://gitee.com/src-openeuler/openstack-glance
- https://gitee.com/src-openeuler/openstack-ironic
- https://gitee.com/src-openeuler/openstack-ironic-python-agent
- https://gitee.com/src-openeuler/openstack-ironic-python-agent-builder
- https://gitee.com/src-openeuler/openstack-ironic-inspector
- https://gitee.com/src-openeuler/openstack-ironic-staging-drivers
- https://gitee.com/src-openeuler/novnc
